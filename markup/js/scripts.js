'use strict';

// scroll animation 
let scrollTo = (target, time) => {
    if(!time) time = 300;
    target = "#" + target;
    $('html, body').animate({scrollTop: $(target).offset().top}, time);
    return false;
    console.log(target);
};